type LocalEnvironment {
  id: Int!
  globalId: ID!
  name: String!
  folderPath: String
  stopPath: String
  deletePath: String
  retryUrl: String
  autoStopPath: String
}

input LocalEnvironmentInput {
  id: Int!
  globalId: ID!
  name: String!
  folderPath: String
  stopPath: String
  deletePath: String
  retryUrl: String
  autoStopPath: String
}

type NestedLocalEnvironment {
  name: String!
  size: Int!
  latest: LocalEnvironment!
}

input NestedLocalEnvironmentInput {
  name: String!
  size: Int!
  latest: LocalEnvironmentInput!
}

type LocalEnvironmentFolder {
  environments: [LocalEnvironment!]!
  availableCount: Int!
  stoppedCount: Int!
}

type ReviewApp {
  canSetupReviewApp: Boolean!
  allClustersEmpty: Boolean!
  reviewSnippet: String
}

type LocalEnvironmentApp {
  stoppedCount: Int!
  availableCount: Int!
  environments: [NestedLocalEnvironment!]!
  reviewApp: ReviewApp!
}

type LocalErrors {
  errors: [String!]!
}

type LocalPageInfo {
  total: Int!
  perPage: Int!
  nextPage: Int!
  previousPage: Int!
}

type k8sPodStatus {
  phase: String
}

type LocalK8sPods {
  status: k8sPodStatus
}

input LocalConfiguration {
  basePath: String
  baseOptions: JSON
}

extend type Query {
  environmentApp(page: Int, scope: String): LocalEnvironmentApp
  folder(environment: NestedLocalEnvironmentInput): LocalEnvironmentFolder
  environmentToDelete: LocalEnvironment
  pageInfo: LocalPageInfo
  environmentToRollback: LocalEnvironment
  environmentToStop: LocalEnvironment
  isEnvironmentStopping(environment: LocalEnvironmentInput): Boolean
  isLastDeployment(environment: LocalEnvironmentInput): Boolean
  k8sPods(configuration: LocalConfiguration, namespace: String): [LocalK8sPods]
}

extend type Mutation {
  stopEnvironment(environment: LocalEnvironmentInput): LocalErrors
  deleteEnvironment(environment: LocalEnvironmentInput): LocalErrors
  rollbackEnvironment(environment: LocalEnvironmentInput): LocalErrors
  cancelAutoStop(autoStopUrl: String!): LocalErrors
  setEnvironmentToDelete(environment: LocalEnvironmentInput): LocalErrors
  setEnvironmentToRollback(environment: LocalEnvironmentInput): LocalErrors
  setEnvironmentToStop(environment: LocalEnvironmentInput): LocalErrors
  setEnvironmentToChangeCanary(environment: LocalEnvironmentInput, weight: Int): LocalErrors
  action(environment: LocalEnvironmentInput): LocalErrors
}
